# Seaonal Greetings #

A simple page that you can put up to remind you of the holiday season.

## Live webpage ##

[https://projects.gregoryhammond.ca/seasonal-greetings/](https://projects.gregoryhammond.ca/seasonal-greetings/)

## License ##

[Unlicense](https://unlicense.org/)

## Special thanks to ##

[Matt Carlson for the inspiration](https://dribbble.com/shots/14864508-Clif-Bar-Seasonal-Flavor-Illustration)

[HTML5 Boilerplate team for their homepage template](https://html5boilerplate.com/)